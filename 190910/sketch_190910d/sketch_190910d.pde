import geomerative.*;

RShape grp;
RPoint[] points;
Particle[] particles;

void setup(){
  size(600,400);
  frameRate(24);
  background(255);
  noStroke();
  fill(0);
  smooth();
  
  RG.init(this);
  grp = RG.getText("Processing!", "FreeSans.ttf", 72, CENTER);
  
  RG.setPolygonizer(RG.UNIFORMLENGTH);
  RG.setPolygonizerLength(2);
  points = grp.getPoints();
  
  if (points != null) {
    particles = new Particle[points.length];
    for (int i = 0; i < points.length; i++) {
      RPoint p = points[i];
      particles[i] = new Particle(p.x, p.y);  
    }
  }
  
  noLoop();
}

void mouseClicked() {
  redraw();
  //for (int i = 0; i < particles.length; i++) {
  //  Particle p = particles[i];
  //  p.reverse();
  //}
}

void draw(){
  background(255);
  translate(width/2, height/2);
  
  for (int i = 0; i < particles.length; i++) {
    Particle p = particles[i];
    p.update();
    p.draw();
  }
}
