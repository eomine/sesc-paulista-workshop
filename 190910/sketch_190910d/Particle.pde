class Particle {
  float x;
  float y;
  float vx;
  float vy;
  
  Particle(float x, float y) {
    this.x = x;
    this.y = y;
    vx = random(-2, 2);
    vy = random(-2, 2);
  }
  
  void reverse() {
    vx *= -1;
    vy *= -1;
  }

  void update() {
    x += vx;
    y += vy;
  }
  
  void draw() {
    pushMatrix();
    translate(x, y);
    ellipse(0, 0, 2, 2);
    popMatrix();
  }
}
