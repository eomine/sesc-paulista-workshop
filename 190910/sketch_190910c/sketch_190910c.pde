// arrays
final int NUM_PARTICLES = 100;
Particle[] particles = new Particle[NUM_PARTICLES];
PFont font;

void setup() {
  size(640, 640);
  font = loadFont("LiberationMono-Bold-96.vlw");
  for (int i = 0; i < NUM_PARTICLES; i=i+1) {
    particles[i] = new Particle();
  }
}

void draw() {
  background(0);
  textFont(font);
  for (int i = 0; i < NUM_PARTICLES; i=i+1) {
    Particle p = particles[i];
    p.updatePosition();
    p.draw();
  }
}
