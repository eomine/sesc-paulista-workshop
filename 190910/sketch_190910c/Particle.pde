class Particle {
  float x;
  float y;
  float vx;
  float vy;
  float ay;
  float size;
  String letter;

  // construtor
  Particle() {
    x = width / 2;
    y = height /2;
    vx = random(-5, 5);
    vy = random(-5, 5);
    ay = 0.05;
    size = random(0.1, 1);
    
    int r = round(random(65, 122));
    letter = Character.toString((char) r);
  }
  
  void updatePosition() {
    vy += ay;
    x += vx;
    y += vy;
    
    if (x < 0 || x > width) {
      vx *= -1;
    }
    if (y < 0 || y > height) {
      vy *= -0.9;
    }
    
    if (mousePressed) {
      float dx = mouseX - x;
      float dy = mouseY - y;
      x += dx * 0.2;
      y += dy * 0.2;
    }
  }
  
  void draw() {
    fill(255);
    pushMatrix();
    translate(x, y);
    scale(size);
    //ellipse(0, 0, size, size);
    //rect(-10, -10, 20, 20);
    text(letter, 0, 0);
    popMatrix();
  }
}
