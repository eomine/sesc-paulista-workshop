// arrays
final int NUM_PARTICLES = 100;
float[] x = new float[NUM_PARTICLES];
float[] y = new float[NUM_PARTICLES];
float[] vx = new float[NUM_PARTICLES];
float[] vy = new float[NUM_PARTICLES];

void setup() {
  size(640, 640);
  for (int i = 0; i < NUM_PARTICLES; i=i+1) {
    x[i] = width / 2;
    y[i] = height /2;
    vx[i] = random(-5, 5);
    vy[i] = random(-5, 5);
  }
}

void draw() {
  background(0);
  for (int i = 0; i < NUM_PARTICLES; i=i+1) {
    x[i] += vx[i];
    y[i] += vy[i];
    
    if (x[i] < 0 || x[i] > width) {
      vx[i] *= -1;
    }
    if (y[i] < 0 || y[i] > height) {
      vy[i] *= -1;
    }
    
    fill(255);
    pushMatrix();
    translate(x[i], y[i]);
    ellipse(0, 0, 20, 20);
    popMatrix();
  }
}
