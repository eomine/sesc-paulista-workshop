// imagem de referência
// "homenagem a volpi", geraldo de barros
// http://enciclopedia.itaucultural.org.br/obra62728/homenagem-a-volpi

void setup() {
  size(640, 640);
  noLoop();
}

void draw() {
  background(0);
  
  for(int i = 0; i < 4; i++) {
    fill(255);
    quad(width/2, 0, width, height/2, width/2, height, 0, height/2);
    translate(width/4, height/4);
    
    if (i < 3) {
      fill(0);
      rect(0, 0, width/2, height/2);
    }
    
    float d = width/4;
    if (i > 0) {
      d *= -1;
    }
    translate(d, 0);
    scale(0.5);
  }
}
