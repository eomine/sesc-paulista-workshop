float x = 0f;
float y = 0f;
float vx = 1f;
float vy = 1f;
float a = 0f;
float va = 1f;

void setup() {
  size(640, 480);
  noStroke();
}

void draw() {
  x = x + vx;
  y = sin(radians(x)) * 100;
  a = a + va;

  fill(0, 10);
  rect(0, 0, width, height);
  
  int size = 25;
  int fillColor = color(255);
  
  if (mousePressed) {
    size = 50;
    fillColor = color(mouseX, mouseY, 0);
  }
  
  fill(fillColor);
  pushMatrix();
  translate(0, height/2);
  translate(x, y);
  rotate(radians(a));
  rect(-size/2, -size/2, size, size);
  popMatrix();
}
