float x = 0f;
float y = 0f;
float vx = 1f;
float vy = 1f;

void setup() {
  size(640, 480);
  noStroke();
}

void draw() {
  x = x + vx;
  y = y + vy;
  if (x < 0 || x > width) {
    vx = vx * -1;
  }
  if (y < 0 || y > height) {
    vy = vy * -1;
  }

  background(0, 0, 0);
  fill(0, 10);
  rect(0, 0, width, height);
  
  int size = 25;
  int fillColor = color(255);
  
  if (mousePressed) {
    size = 50;
    fillColor = color(mouseX, mouseY, 0);
  }
  
  fill(fillColor);
  pushMatrix();
  translate(x, y);
  rotate(radians(45));
  rect(-size/2, -size/2, size, size);
  popMatrix();
}
