void setup() {
  size(640, 480);
  background(0);
  noStroke();
}

void draw() {
  int size = 25;
  int fillColor = color(255);
  
  if (mousePressed) {
    size = 50;
    fillColor = color(mouseX, mouseY, 0);
  }
  
  fill(fillColor);
  ellipse(mouseX, mouseY, size, size);
}
