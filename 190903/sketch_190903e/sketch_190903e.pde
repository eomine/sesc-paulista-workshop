final int NUM_COLS = 20;
final int NUM_ROWS = 20;
final color BG_COLOR = #587383;
final color PINK_COLOR = #ff6a89;
final color YELLOW_COLOR = #f4f26c;
float unitWidth;
float unitHeight;

void setup() {
  size(640, 640);
  unitWidth = width / NUM_COLS;
  unitHeight = height / NUM_ROWS;
  noLoop();
  noFill();
  strokeWeight(2);
}

void draw() {
  background(BG_COLOR);
  for (int i = 0; i < NUM_COLS; i=i+1) {
    for (int j = 0; j < NUM_ROWS; j=j+1) {
      pushMatrix();
      translate(i * unitWidth, j * unitHeight);
      drawUnit(j);
      popMatrix();
    }
  }
}

void mouseClicked() {
  redraw();
}

void drawUnit(int j) {
  int r = round(random(1, NUM_ROWS));
  if (r > j) {
    drawPinkCross();
  } else {
    stroke(YELLOW_COLOR);
    int r2 = round(random(1, 2));
    if (r2 == 1) {
      drawSquare();
    } else {
      drawLozenge();
    }
  }
}

void drawPinkCross() {
  stroke(PINK_COLOR);
  line(unitWidth / 2, 0, unitWidth / 2, unitHeight);
  line(0, unitHeight / 2, unitWidth, unitHeight / 2);
}

void drawSquare() {
  rect(0, 0, unitWidth, unitHeight);
}

void drawLozenge() {
  quad(
    unitWidth / 2, 0,
    unitWidth, unitHeight / 2,
    unitWidth / 2, unitHeight,
    0, unitHeight / 2
  );
}
