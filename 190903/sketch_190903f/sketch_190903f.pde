final int NUM_COLS = 14;
final int NUM_ROWS = 14;
final color BG_COLOR = #ffffff;
final color STROKE_COLOR = #000000;
final color PINK_COLOR = #f1d0cc;
final color BLUE_COLOR = #2919d0;
final color GREEN_COLOR = #556978;
float unitWidth;
float unitHeight;

void setup() {
  size(640, 640);
  unitWidth = (float) width / NUM_COLS;
  unitHeight = (float) height / NUM_ROWS;
  noLoop();
  stroke(STROKE_COLOR);
  strokeWeight(2);
  strokeJoin(ROUND);
}

void mouseClicked() {
  redraw();
}

void draw() {
  background(BG_COLOR);
  for (int i = 0; i < NUM_COLS; i=i+1) {
    for (int j = 0; j < NUM_ROWS; j=j+1) {
      pushMatrix();
      translate(i * unitWidth, j * unitHeight);
      setColor();
      drawUnit();
      popMatrix();
    }
  }
}

void setColor() {
  int r3 = round(random(1, 3));
  color fillColor = PINK_COLOR;
  if (r3 == 1) {
    fillColor = BLUE_COLOR;
  } else if (r3 == 2) {
    fillColor = GREEN_COLOR;
  }
  fill(fillColor);
}

void drawUnit() {
  int r = round(random(1, 2));
  if (r == 1) {
    int r2 = round(random(1, 6));
    switch (r2) {
      case 1:
        drawSquare();
        break;
      case 2:
        drawCircle();
        break;
      case 3:
        drawUpTriangle();
        break;
      case 4:
        drawDownTriangle();
        break;
      case 5:
        drawLeftTriangle();
        break;
      case 6:
        drawRightTriangle();
        break;
    }
  }
}

void drawSquare() {
  rect(0, 0, unitWidth, unitHeight);
}

void drawCircle() {
  ellipse(unitWidth / 2, unitHeight / 2, unitWidth / 2, unitHeight / 2);
}

void drawUpTriangle() {
  triangle(
    0, 0,
    unitWidth / 2, unitHeight / 2,
    unitWidth, 0
  );
}

void drawDownTriangle() {
  triangle(
    0, unitHeight,
    unitWidth / 2, unitHeight / 2,
    unitWidth, unitHeight
  );
}

void drawLeftTriangle() {
  triangle(
    0, 0,
    unitWidth / 2, unitHeight / 2,
    0, unitHeight
  );
}

void drawRightTriangle() {
  triangle(
    unitWidth, 0,
    unitWidth / 2, unitHeight / 2,
    unitWidth, unitHeight
  );
}
