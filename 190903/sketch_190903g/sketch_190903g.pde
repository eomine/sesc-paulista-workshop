void setup() {
  size(640, 640);
  noLoop();
}

void draw() {
  background(#ffffff);
  for (int i = 0; i < 20; i++) {
    randomizeColor();
    drawLine();
  }
}

void mouseClicked() {
  redraw();
}

void randomizeColor() {
  int r = round(random(1, 3));
  color c = #ff0000;
  switch(r) {
    case 1:
      c = #ffff00;
      break;
    case 2:
      c = #0000ff;
      break;
  }
  stroke(c);
  strokeWeight(10);
}

void drawLine() {
  int r = round(random(1, 2));
  if (r == 1) {
      drawVerticalLine();
  } else {
      drawHorizontalLine();
  }
}

void drawVerticalLine() {
  float x = random(width);
  line(x, 0, x, height);
}

void drawHorizontalLine() {
  float y = random(height);
  line(0, y, width, y);
}
