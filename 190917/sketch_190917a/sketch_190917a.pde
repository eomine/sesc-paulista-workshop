PImage photo;

void setup() {
  size(530, 620);
  photo = loadImage("abaporu.jpg");
  photo.loadPixels();
  background(0);
  noStroke();
}

void draw() {
  float d = random(30, 50); 
  color c = photo.pixels[mouseX + mouseY * width];
  fill(c, 200);
  rect(mouseX, mouseY, d, d);
}
