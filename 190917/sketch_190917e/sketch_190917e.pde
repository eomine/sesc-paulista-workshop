import processing.video.*;
Capture cam;

final int NUM_COLS = 32;
final int NUM_ROWS = 24;
float unitWidth;
float unitHeight;

void setup() {
  size(640, 480);
  unitWidth = width / NUM_COLS;
  unitHeight = height / NUM_ROWS;

  cam = new Capture(this, 640, 480);
  cam.start();
}

void draw() {
  if (cam.available()) {
    cam.read();
  }

  background(0);
  for (int i = 0; i < NUM_COLS; i++) {
    for (int j = 0; j < NUM_ROWS; j++) {
      int x = round(i * unitWidth);
      int y = round(j * unitHeight);
      color c = cam.get(x, y);
      float w = brightness(c) / 10;
      stroke(c);
      strokeWeight(w);
      pushMatrix();
      translate(width - x, y);
      scale(0.5);
      line(0, 0, unitWidth, unitHeight);
      popMatrix();
    }
  }
}
