final int NUM_COLS = 20;
final int NUM_ROWS = 24;
float unitWidth;
float unitHeight;
PImage photo;

void setup() {
  size(530, 620);
  unitWidth = width / NUM_COLS;
  unitHeight = height / NUM_ROWS;
  photo = loadImage("abaporu.jpg");
  photo.loadPixels();
  noFill();
  noLoop();
}

void draw() {
  background(0);
  for (int i = 0; i < NUM_COLS; i++) {
    for (int j = 0; j < NUM_ROWS; j++) {
      int x = round(i * unitWidth);
      int y = round(j * unitHeight);
      color c = photo.pixels[x + y * width];
      float w = random(5, 15);
      stroke(c);
      strokeWeight(w);
      pushMatrix();
      translate(x, y);
      line(0, 0, unitWidth, unitHeight);
      popMatrix();
    }
  }
}
