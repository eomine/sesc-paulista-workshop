final int NUM_COLS = 30;
final int NUM_ROWS = 34;
float unitWidth;
float unitHeight;
float n = 0f;
PImage photo;

void setup() {
  size(530, 620);
  unitWidth = width / NUM_COLS;
  unitHeight = height / NUM_ROWS;
  photo = loadImage("abaporu.jpg");
  photo.loadPixels();
  noStroke();
  noLoop();
}

void draw() {
  background(0);
  for (int i = 0; i < NUM_COLS; i++) {
    for (int j = 0; j < NUM_ROWS; j++) {
      int x = round(i * unitWidth);
      int y = round(j * unitHeight);
      color c = photo.pixels[x + y * width];
      n += 0.01f;
      float w = noise(n) * 20;
      fill(c);
      pushMatrix();
      translate(x, y);
      ellipse(0, 0, w, w);
      popMatrix();
    }
  }
}
