int[] pointsA = {
  1, 1, 0, 0,
  0, 0, 1, 0,
  0, 1, 1, 0,
  1, 0, 1, 0,
  0, 1, 0, 1
};

void setup() {
  size(200, 500);
  final int NUM_COLS = 4;
  final int NUM_ROWS = 5;
  int unitWidth = width / NUM_COLS;
  int unitHeight = height / NUM_ROWS;
}

void drawLetter(int x0, int y0, int w, int h) {
  for (int i = 0; i < 20; i++) {
    int point = pointsA[i];
    if (point == 1) {
      int x = i % 4; // resto da divisão do i / largura;
      int y = floor(i / 4); // divisao do i / largura arred baixo;
      pushMatrix();
      scale(unitWidth, unitHeight);
      translate(x, y);
      //translate(0.5, 0.5);
      noStroke();
      rect(0, 0, 1, 1);
      popMatrix();
    }
  }
}

void draw() {
  drawLetter(10, 10, 200, 50);
  drawLetter(50, 100, 50, 200);
}
