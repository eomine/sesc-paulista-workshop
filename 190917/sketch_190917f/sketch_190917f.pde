import processing.video.*;
Capture cam;

final int NUM_COLS = 32;
final int NUM_ROWS = 24;
float unitWidth;
float unitHeight;
PFont font;

String quote = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?";

void setup() {
  size(640, 480);
  unitWidth = width / NUM_COLS;
  unitHeight = height / NUM_ROWS;

  cam = new Capture(this, 640, 480);
  cam.start();
  
  font = loadFont("AvenirNext-Bold-48.vlw");
  textFont(font);
  textAlign(CENTER);
}

void draw() {
  if (cam.available()) {
    cam.read();
  }

  background(0);
  for (int i = 0; i < NUM_COLS; i++) {
    for (int j = 0; j < NUM_ROWS; j++) {
      int x = round(i * unitWidth);
      int y = round(j * unitHeight);
      color c = cam.get(x, y);
      float s = brightness(c) / 128;
      fill(c);
      pushMatrix();
      translate(x, y);
      translate(unitWidth/2, unitHeight/2);
      scale(0.5);
      scale(s);
      text(quote.charAt(i + j * NUM_COLS), 0, 0);
      popMatrix();
    }
  }
}
