final int NUM_COLS = 30;
final int NUM_ROWS = 24;
float unitWidth;
float unitHeight;
PImage image;
PFont font;
String quote = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?";

void setup() {
  size(900, 600);
  unitWidth = width / NUM_COLS;
  unitHeight = height / NUM_ROWS;
  image = loadImage("masp.jpg");
  image.loadPixels();
  font = loadFont("Corbel-Bold-48.vlw");
  noStroke();
  noLoop();
}

void mouseClicked() {
  redraw();
}

void draw() {
  background(0);
  textFont(font, 48);
  textAlign(CENTER);
  float n = 0;
  for (int i = 0; i < NUM_COLS; i++) {
    for (int j = 0; j < NUM_ROWS; j++) {
      float x = i * unitWidth;
      float y = j * unitHeight;
      color c = image.pixels[floor(x) + floor(y) * width];
      fill(c);
      float s = noise(n) * 2;
      n += 0.01;
      pushMatrix();
      translate(x, y);
      translate(unitWidth / 2, unitHeight * 1.5);
      scale(s);
      //drawUnit();
      text(quote.charAt(i + j * NUM_COLS), 0, 0);
      popMatrix();
    }
  }
}

//void drawUnit() {
//  line(0, 0, unitWidth, unitHeight);
//  line(unitWidth, 0, 0, unitHeight);
//  noFill();
//  rect(0, 0, unitWidth, unitHeight);
//}


void shapeSketch0() {
  background(82, 38, 38);
  rect(62.0, 61.0, 197.0, 197.0);
  rect(178.0, 176.0, 192.0, 192.0);
  rect(297.0, 272.0, 169.0, 169.0);
  fill(178, 83, 83);
  rect(300.0, 130.0, 109.0, 109.0);
  stroke(16, 75, 222);
  rect(125.0, 118.0, 59.0, 59.0);
}
