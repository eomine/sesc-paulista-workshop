final int NUM_POINTS = 3000;
PImage photo;

void setup() {
  size(530, 620);
  photo = loadImage("abaporu.jpg");
  photo.loadPixels();
  noStroke();
  noLoop();
}

void draw() {
  background(0);
  for (int i = 0; i < NUM_POINTS; i++) {
    int x = floor(random(0, width));
    int y = floor(random(0, height));
    color c = photo.pixels[x + y * width];
    float a = random(20, 200);
    float d = random(20, 50);
    fill(c, a);
    ellipse(x, y, d, d);
  }
}
